package test;
import org.junit.jupiter.api.Test;
import com.company.Tasks;
import static org.junit.jupiter.api.Assertions.*;

public class Taskstest {
    Tasks tasks = new Tasks();

    @Test
    void gaubitsGradTest1() {
        double expected = 0.787d;
        double actual = tasks.gaubitsGrad(45, 10d);
        assertTrue(Math.abs(expected - actual) < 0.001d);
    }

    @Test
    void gaubitsGradTest2() {
        double expected = 0d;
        double actual = tasks.gaubitsGrad(90, 10d);
        assertTrue(Math.abs(expected - actual) < 0.1d);
    }

    @Test
    void gaubitsRadTest1() {
        double expected = 0.787d;
        double actual = tasks.gaubitsRad(3.14/4, 10d);
        assertTrue(Math.abs(expected - actual) < 0.001d);
    }

    @Test
    void gaubitsRadTest2() {
        double expected = 0.0d;
        double actual = tasks.gaubitsRad(3.14/2, 10d);
        assertTrue(Math.abs(expected - actual) < 0.1d);
    }
//    ******************************************************

    @Test
    void task2Test1() {
        double expected = 30d;
        double actual = tasks.task2(5d, 10, 15, 1);
        assertTrue(Math.abs(expected - actual) < 0.1d);
    }

    @Test
    void task2Test2() {
        double expected = -1d;
        double actual = tasks.task2(5d, -10, 15, 1);
        assertTrue(Math.abs(expected - actual) < 0.1d);
    }
    //    *********************************************
    @Test
    void task3Test1() {
        boolean expected = true;
        boolean actual = tasks.task3(0d,0d);
        assertTrue(expected == actual);
    }

    @Test
    void task3Test2() {
        boolean expected = true;
        boolean actual = tasks.task3(0.5d,0d);
        assertTrue(expected == actual);
    }

    //    *****************************************
    @Test
    void task4Test1() {
        assertTrue(tasks.task4(Math.PI - 0.1d).isNaN());
    }

    @Test
    void task4Test2() {
        assertTrue(!tasks.task4(2 * Math.PI - 0.1d).isNaN());
    }

}







