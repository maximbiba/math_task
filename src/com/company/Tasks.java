package com.company;

public class Tasks {
    public double gaubitsGrad(double alpha, double speed) {
        double aInRad = alpha * 3.14d / 180.0d;
        return gaubitsRad(aInRad, speed);
    }

    public double gaubitsRad(double alpha, double speed) {
        double vInMetersOnSeconds = speed / 3.6;
        double result = (2.0 * vInMetersOnSeconds * vInMetersOnSeconds * Math.sin(alpha) * Math.cos(alpha)) / 9.8;
        return result;
    }

    public double task2(double s, double v1, double v2, double t) {
        if (s < 0 || v1 < 0 || v2 < 0 || t < 0) {
            System.out.println("Incorrect number");
            return -1.0d;
        }
        return s + (v1 + v2) * t;
    }

    public boolean task3(double x, double y) {
        if (x >= y && x <= (2d * y / 3d + 2d / 3d) && x >= 0d) {
            return true;
        }
        if (-x >= y && -x <= (2d * y / 3d + 2d / 3d) && x <= 0d) {
            return true;
        }
        return false;
    }

    public Double task4(double x) {
        double a = Math.exp(x + 1) + 2d * Math.exp(x) * Math.cos(x);
        double b = x - Math.exp(x + 1) * Math.sin(x);
        double result = (6d * Math.log1p(Math.sqrt(a))) / (Math.log1p(b)) + Math.abs(Math.cos(x) / Math.exp(Math.sin(x)));
        return result;

    }
}
